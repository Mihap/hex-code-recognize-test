﻿using UnityEngine;
using System.Collections.Generic;
using System;
using OpenCVForUnity;
using recognTools;
using Quarcode.Core;
using FaceTrackerSample;
using System.Threading;
using System.IO;

[RequireComponent(typeof(WebCamTextureToMatHelper))]
public class HexcodeScannerWeb : MonoBehaviour, IRecieveCroptedImage, IRecieveFilteredImage, IRecieveFoundContours, IRecieveRawImage, IRecieveRecognizedCode, IRecieveFailFilteredContours
{
    object imageLock = new object();
    bool DoHexFinding = true;
    bool CodeRecognized = false;
    volatile string LastFoundCode = null;
    private void OnCodeFound(string Code)
    {
        _isStartRecogn = true;
        CodeRecognized = true;
        LastFoundCode = Code;

    }
    Color32[] colors;

    Texture2D texture;

    WebCamTextureToMatHelper webCamTextureToMatHelper;

    bool _isStartRecogn;
    Mat _rawImage;

    public Renderer rend;

    Thread AsysncReaderThread;
    // Use this for initialization
    void Start()
    {
        webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper>();
        webCamTextureToMatHelper.Init();
        AsysncReaderThread = new Thread(AsyncCodeFindingFunction);
        ImageParser.AddDevDataReciever(this);
        AsysncReaderThread.Start();
    }

    private void AsyncCodeFindingFunction()
    {
       while (DoHexFinding)
       {
      
            Mat image = null;
            lock (imageLock) {
                image = _rawImage.clone();
                   
            }
        
            //Texture2D texture = new Texture2D(image.cols(), image.rows(), TextureFormat.RGBA32, false);
            //Utils.matToTexture2D(image, texture);
            //byte[] png = texture.EncodeToPNG();
            //string Path = Application.streamingAssetsPath + "/../../out/src.png";
            //File.WriteAllBytes(@Path, png);

            string result = ImageParser.ProcessSync(_rawImage);
            if (result != null)
            {
                //DoHexFinding = false;
                OnCodeFound(result);
            }
            else
            {
                Thread.Sleep(10);
            }
        image.Dispose();
      }
        return;
    }

    void IRecieveCroptedImage.Recieve(Mat source)
    {
        Debug.Log("cropted succesful");
    }

    void IRecieveFilteredImage.Recieve(Mat source)
    {

    }

    void IRecieveFoundContours.Recieve(List<MatOfPoint> contourList)
    {

    }

    void IRecieveRawImage.Recieve(Mat source)
    {
    }

    void IRecieveFailFilteredContours.Recieve()
    {
        _isStartRecogn = false;
    }

    void IRecieveRecognizedCode.Recieve(string code)
    {
        if (code != null)
        {
            string result = string.Format("Code: {0}", code);
            Debug.Log(result);
        }
        else
        {
            Debug.Log("---------No--Code-----------");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (webCamTextureToMatHelper.isPlaying() && webCamTextureToMatHelper.didUpdateThisFrame())
        {
            lock (imageLock)
            {
                Mat rgbaMat = webCamTextureToMatHelper.GetMat();
                if (CodeRecognized && LastFoundCode != null)
                {
                    //логика загрузки кода  

                    // просто выведу его на экран
                    Imgproc.putText(rgbaMat, "Last Recogned Code: " + LastFoundCode, new Point(5, rgbaMat.rows() - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar(255, 255, 255, 255), 2, Imgproc.LINE_AA, false);

                }
                //Imgproc.putText (rgbaMat, "W:" + rgbaMat.width () + " H:" + rgbaMat.height () + " SO:" + Screen.orientation, new Point (5, rgbaMat.rows () - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar (255, 255, 255, 255), 2, Imgproc.LINE_AA, false);
                Utils.matToTexture2D(rgbaMat, texture, colors);
                //_isStartRecogn = true;
                Imgproc.cvtColor(rgbaMat, _rawImage, Imgproc.COLOR_RGBA2BGR);

                //OutputImage(_rawImage);
                //AsyncCodeFindingFunction();
            }
        }
    }


    void OnDisable()
    {
        // If the thread is still running, we should shut it down,
        // otherwise it can prevent the game from exiting correctly.
        if (DoHexFinding) {
            // This forces the while loop in the ThreadedWork function to abort.
            DoHexFinding = false;

            // This waits until the thread exits,
            // ensuring any cleanup we do after this is safe. 
            AsysncReaderThread.Join ();
        }

        webCamTextureToMatHelper.Dispose();
        if (_rawImage != null)
            _rawImage.Dispose();
    }



    public void OnWebCamTextureToMatHelperInited()
    {
        Debug.Log("OnWebCamTextureToMatHelperInited");

        Mat webCamTextureMat = webCamTextureToMatHelper.GetMat();

        colors = new Color32[webCamTextureMat.cols() * webCamTextureMat.rows()];
        texture = new Texture2D(webCamTextureMat.cols(), webCamTextureMat.rows(), TextureFormat.RGBA32, false);

        _rawImage = new Mat(webCamTextureMat.rows(), webCamTextureMat.cols(), CvType.CV_8UC3);

        gameObject.transform.localScale = new Vector3(webCamTextureMat.cols(), webCamTextureMat.rows(), 1);

        Debug.Log("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);

        float width = 0;
        float height = 0;

        width = gameObject.transform.localScale.x;
        height = gameObject.transform.localScale.y;

        float widthScale = (float)Screen.width / width;
        float heightScale = (float)Screen.height / height;
        if (widthScale < heightScale)
        {
            Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
        }
        else
        {
            Camera.main.orthographicSize = height / 2;
        }

        gameObject.GetComponent<Renderer>().material.mainTexture = texture;

    }


    public void SaveScreen()
    {
        byte[] png = ((Texture2D)gameObject.GetComponent<Renderer>().material.mainTexture).EncodeToPNG();
        File.WriteAllBytes(Application.streamingAssetsPath + "/Screenshot.png", png);
    }
    void OutputImage(Mat mat)
    {
        Texture2D texture = new Texture2D(mat.cols(), mat.rows(), TextureFormat.RGBA32, false);
        Utils.matToTexture2D(mat, texture);
        Destroy (rend.material.mainTexture);
        rend.material.mainTexture = texture;

    }

    public void OnWebCamTextureToMatHelperDisposed()
    {
        Debug.Log("OnWebCamTextureToMatHelperDisposed");

    }
}
