﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace Quarcode.Core
{
  public struct SViewState
  {
    public int radius;
    public string Message;
    public bool DrawCellBorder;
    public bool DrawValNum;
    public bool DrawQRBorder;
    public bool FillCells;
    public bool ReRand;
  }

  public enum PointType
  {
    ByteTrue,
    ByteFalse,
    UndefinedByte,
    Logo,
    Border,
    LogoAdditional
  }

  public class CGexPoint
  {
    public PointType pointType;

    public Vector r;

    public List<Vector> Cell;

    public CGexPoint(PointType type, Vector center, List<Vector> Surround)
    {
      pointType = type;
      r = center;
      Cell = Surround;
    }

    public void SetType(PointType type)
    {
      this.pointType = type;
    }
  }

  public static class Common
  {
    public static Color32 createColor(XmlElement elem)
    {
      byte r = 0, g = 0, b = 0, a = 255;

      byte.TryParse(elem.GetAttribute("r"), out r);
      byte.TryParse(elem.GetAttribute("g"), out g);
      byte.TryParse(elem.GetAttribute("b"), out b);
      byte.TryParse(elem.GetAttribute("a"), out a);
      Color32 result = new Color32(r, g, b, a);
      return result;
    }
  }
}
