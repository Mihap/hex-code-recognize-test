﻿using UnityEngine;
using System.Collections.Generic;
using System;
using OpenCVForUnity;
using recognTools;
using Quarcode.Core;
using FaceTrackerSample;
using System.Threading;
using System.IO;
using UnityEngine.UI;

[RequireComponent(typeof(WebCamTextureToMatHelper))]
public class HexcodeScannerWebTheard : MonoBehaviour
{

  public RawImage imageDebug;
  public RawImage imageDebug2;
  Color32[] colors;

  Texture2D texture;

  WebCamTextureToMatHelper webCamTextureToMatHelper;

  bool _isStartRecogn;
  Mat _rawImage;

  public Renderer rend;

  private volatile bool shouldStopThread = false;

  private volatile ThreadComm threadComm = new ThreadComm();
  private System.Object thisLock = new System.Object();
  private volatile bool isThreadRunning = false;
  private volatile bool didUpdateTheDetectionResult = false;
  private Mat grayMat4Thread;
  private string resultDetect;
  private string lastResultDetect;


  public Texture2D imgTexture;


  public bool debugModeImage = true;
  // Use this for initialization
  void Start()
  {
    // _rawImage = new Mat ();
    webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper>();
    webCamTextureToMatHelper.Init();
    // AsysncReaderThread = new Thread(AsyncCodeFindingFunction);
    // AsysncReaderThread.Start();
    if (debugModeImage)
    {
      _rawImage = new Mat(imgTexture.height, imgTexture.width, CvType.CV_8UC3);
      Utils.texture2DToMat(imgTexture, _rawImage);
      string result = ImageParser.ProcessSync(_rawImage);
      Debug.Log(result);
    }
  }


  // Update is called once per frame
  void Update()
  {

    if (webCamTextureToMatHelper.isPlaying() && webCamTextureToMatHelper.didUpdateThisFrame())
    {
      Mat rgbaMat = webCamTextureToMatHelper.GetMat();


      //Imgproc.cvtColor (rgbaMat, _rawImage, Imgproc.COLOR_RGBA2BGR);
      //Imgproc.putText (rgbaMat, "Last Recogned Code: " + lastResultDetect, new Point (5, rgbaMat.rows () - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar (255, 255, 255, 255), 2, Imgproc.LINE_AA, false);
      if (!threadComm.shouldDetectInMultiThread)
      {


        Imgproc.cvtColor(rgbaMat, grayMat4Thread, Imgproc.COLOR_RGBA2BGR);
        //_rawImage.copyTo (grayMat4Thread);
        threadComm.shouldDetectInMultiThread = true;
      }

      if (didUpdateTheDetectionResult)
      {
        didUpdateTheDetectionResult = false;
        lock (thisLock)
        {

          lastResultDetect = resultDetect;
        }

      }

      if (lastResultDetect != null && lastResultDetect != "")
      {
        //Debug.Log(lastResultDetect);
        Imgproc.putText(rgbaMat, "Code: " + lastResultDetect, new Point(5, rgbaMat.rows() - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar(255, 255, 255, 255), 2, Imgproc.LINE_AA, false);
        if (shouldStopThread == false)
        {
          //StopThread ();
        }

      }

      Utils.matToTexture2D(rgbaMat, texture, colors);
      OutputImage(HexDecoder.Filter(grayMat4Thread));
      OutputImage(ImageParser.CroptedImage, 1);

    }
  }


  void OnDisable()
  {


    webCamTextureToMatHelper.Dispose();

    StopThread();

    if (grayMat4Thread != null)
      grayMat4Thread.Dispose();

    if (_rawImage != null)
      _rawImage.Dispose();
  }



  public void OnWebCamTextureToMatHelperInited()
  {
    //Debug.Log("OnWebCamTextureToMatHelperInited");

    Mat webCamTextureMat = webCamTextureToMatHelper.GetMat();

    colors = new Color32[webCamTextureMat.cols() * webCamTextureMat.rows()];
    texture = new Texture2D(webCamTextureMat.cols(), webCamTextureMat.rows(), TextureFormat.RGBA32, false);

    _rawImage = new Mat(webCamTextureMat.rows(), webCamTextureMat.cols(), CvType.CV_8UC3);



    initThread();

    gameObject.transform.localScale = new Vector3(webCamTextureMat.cols(), webCamTextureMat.rows(), 1);

    //Debug.Log("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);

    float width = 0;
    float height = 0;

    width = gameObject.transform.localScale.x;
    height = gameObject.transform.localScale.y;

    float widthScale = (float)Screen.width / width;
    float heightScale = (float)Screen.height / height;
    if (widthScale < heightScale)
    {
      Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
    }
    else
    {
      Camera.main.orthographicSize = height / 2;
    }

    gameObject.GetComponent<Renderer>().material.mainTexture = texture;

  }


  private void initThread()
  {
    StopThread();

    grayMat4Thread = new Mat();
    threadComm.shouldDetectInMultiThread = false;

    StartThread();
  }

  private void ThreadWorker()
  {
    if (isThreadRunning)
      return;

    Debug.Log("Thread Start");

    isThreadRunning = true;
    shouldStopThread = false;

    threadComm.shouldDetectInMultiThread = false;
    didUpdateTheDetectionResult = false;

    ThreadPool.QueueUserWorkItem(_ThreadWorker, threadComm);
  }

  private void _ThreadWorker(System.Object o)
  {
    ThreadComm comm = o as ThreadComm;


    while (!shouldStopThread)
    {
      if (!comm.shouldDetectInMultiThread)
        continue;

      string result = "Not Found";
      try
      {
        result = ImageParser.ProcessSync(grayMat4Thread);
      }
      catch(Exception e)
      {

      }

            //Thread.Sleep(20);
      lock (thisLock)
      {
        resultDetect = result;
      }
      comm.shouldDetectInMultiThread = false;

      didUpdateTheDetectionResult = true;
    }

    isThreadRunning = false;
  }

  public class ThreadComm : System.Object
  {
    public bool shouldDetectInMultiThread = false;
  }


  public void StartThread()
  {
    ThreadWorker();
  }

  public void StopThread()
  {
    return;
    if (!isThreadRunning)
      return;

    shouldStopThread = true;



    while (isThreadRunning)
    {
      //Wait threading stop
    }
    Debug.Log("Thread Stop");
  }



  public void SaveScreen()
  {
    byte[] png = ((Texture2D)gameObject.GetComponent<Renderer>().material.mainTexture).EncodeToPNG();
    File.WriteAllBytes(Application.streamingAssetsPath + "/Screenshot.png", png);
  }
  void OutputImage(Mat mat, int n = 0)
  {
    if (mat == null) return;

    Texture2D texture = new Texture2D(mat.cols(), mat.rows(), TextureFormat.RGBA32, false);
    Utils.matToTexture2D(mat, texture);
    if (n == 0)
    {
      Destroy(imageDebug.texture);
      imageDebug.texture = texture;
    }
    else if (n == 1)
    {
      Destroy(imageDebug2.texture);
      imageDebug2.texture = texture;

    }
    //rend.material
    // Destroy (rend.material.mainTexture);
    //rend.material.mainTexture = texture;

  }

  public void OnWebCamTextureToMatHelperDisposed()
  {
    Debug.Log("OnWebCamTextureToMatHelperDisposed");

  }
}
