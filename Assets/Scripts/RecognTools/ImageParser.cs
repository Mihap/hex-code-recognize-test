﻿using System;
using System.Collections.Generic;
using OpenCVForUnity;
using UnityEngine;
using System.IO;

namespace recognTools
{
    public delegate void OnHexCodeRecognizedDelegate(string code);
    public delegate void OnImageRecievedDelegate(Mat sourceBgr);
    public delegate void OnImageFilteredDelegate(Mat sourceHsv);
    public delegate void OnContourFoundDelegate(List<MatOfPoint> contourList);
    public delegate void OnFailContourFilteredDelegate();
    public delegate void OnHexImageCroptedDelegate(Mat sourceBgr);

    public static class ImageParser
    {
        public static event OnHexCodeRecognizedDelegate OnHexCodeRecognized;
        public static event OnImageRecievedDelegate OnImageRecieved;
        public static event OnImageFilteredDelegate OnImageFiltered;
        public static event OnContourFoundDelegate OnContourFound;
        public static event OnFailContourFilteredDelegate OnFailContourFiltered;
        public static event OnHexImageCroptedDelegate OnHexImageCropted;

        private static int n = 0;
        public static Mat FilteredImage = null;
        public static Mat CroptedImage = null;

        public static void AddDevDataReciever(object reciever)
        {
            var recieveRecognizedCode = reciever as IRecieveRecognizedCode;
            if (recieveRecognizedCode != null)
                OnHexCodeRecognized += recieveRecognizedCode.Recieve;
            var recieveRawImage = reciever as IRecieveRawImage;
            if (recieveRawImage != null)
                OnImageRecieved += recieveRawImage.Recieve;
            var recieveFilteredImage = reciever as IRecieveFilteredImage;
            if (recieveFilteredImage != null)
                OnImageFiltered += recieveFilteredImage.Recieve;
            var recieveFoundContours = reciever as IRecieveFoundContours;
            if (recieveFoundContours != null)
                OnContourFound += recieveFoundContours.Recieve;
            var recieveFailContoursFiltered = reciever as IRecieveFailFilteredContours;
            if (recieveFailContoursFiltered != null)
                OnFailContourFiltered += recieveFailContoursFiltered.Recieve;
            var recieveCroptedImage = reciever as IRecieveCroptedImage;
            if (recieveCroptedImage != null)
                OnHexImageCropted += recieveCroptedImage.Recieve;
        }

        public static string ProcessSync(Mat source)
        {
            n++;
            // 0. Scale
            //const int WorkWidth = 800;
            //double scale = WorkWidth / (double)source.width ();
            //Imgproc.resize (source, source, new Size (source.width () * scale, source.height () * scale), 0f, 0f, Imgproc.INTER_LINEAR);

            // 1. Recieve
            //Imgproc.medianBlur (source, source, 9);

            // 2.Filter
            Mat filtredsource = HexDecoder.Filter(source);
            if (FilteredImage != null)
                FilteredImage.Dispose();

            //3.Find All Contours
            List<MatOfPoint> allContours = HexDecoder.FindAllContours(filtredsource);

            // 4.Filter Contours
            List<MatOfPoint> fltContours = new List<MatOfPoint>();
            fltContours.AddRange(HexDecoder.FilterAllContours(allContours));
            if (fltContours.Count < 2)
            {
                fltContours.ForEach(x => x.Dispose());
                fltContours.Clear();
                allContours.ForEach(x => x.Dispose());
                allContours.Clear();
                filtredsource.Dispose();
                //UnityEngine.Debug.Log("не найдены два рапозноваемых контура");
                return null;
            }
            else
            {
                //UnityEngine.Debug.Log("Конутры найдены");
            }

            // 5.Crop true Orinted HexImage
            Mat CropMatrix = HexDecoder.CropCodeFromImage(source, fltContours);
            //if (CroptedImage != null)
           

            // 6.Parse Image for Code

            string result = HexDecoder.TryDecode(CropMatrix);

            #region Export to texture if main thread
            //texture = new Texture2D(CropMatrix.cols(), CropMatrix.rows(), TextureFormat.RGBA32, false);
            //Utils.matToTexture2D(CropMatrix, texture);
            //png = texture.EncodeToPNG();
            //if (result == null)
            //    Path = Application.streamingAssetsPath + "/../../out/" + n + "_crop.png";
            //else
            //    Path = Application.streamingAssetsPath + "/../../out/" + n + "_" + result + "_crop.png";

            //File.WriteAllBytes(@Path, png);
            #endregion
            CroptedImage = CropMatrix.clone();
            CropMatrix.Dispose();
            fltContours.ForEach(x => x.Dispose());
            fltContours.Clear();
            allContours.ForEach(x => x.Dispose());
            allContours.Clear();
            filtredsource.Dispose();
            //source.Dispose();
            return result;
        }

        public static void RecieveImage(Mat source)
        {
            // 0. Scale
            const int WorkWidth = 1000;
            double scale = WorkWidth / (double)source.width();
            Imgproc.resize(source, source, new Size(source.width() * scale, source.height() * scale), 0f, 0f, Imgproc.INTER_LANCZOS4);

            // 1. Recieve
            Imgproc.medianBlur(source, source, 9);
            OnImageRecieved(source);

            // 2.Filter
            Mat filtredsource = HexDecoder.Filter(source);
            if (OnImageRecieved != null)
                OnImageFiltered(filtredsource);

            // 3.Find All Contours
            List<MatOfPoint> allContours = HexDecoder.FindAllContours(filtredsource);
            if (OnContourFound != null)
                OnContourFound(allContours);

            // 4.Filter Contours
            List<MatOfPoint> fltContours = HexDecoder.FilterAllContours(allContours);
            //Imgproc.drawContours (source, allContours, -1, new Scalar (255, 0, 0), 2);
            if (fltContours.Count < 2)
            {
                UnityEngine.Debug.Log("не найдены два рапозноваемых контура");
                if (OnFailContourFiltered != null)
                    OnFailContourFiltered();
                return;
                throw new Exception("не найдены два рапозноваемых контура");
            }

            // 5.Crop true Orinted HexImage
            Mat CropMatrix = HexDecoder.CropCodeFromImage(source, fltContours);

            if (OnHexImageCropted != null)
                OnHexImageCropted(CropMatrix);

            // 6.Parse Image for Code
            string result = HexDecoder.TryDecode(CropMatrix);
            //if (result != null)
            if (OnHexCodeRecognized != null)
                OnHexCodeRecognized(result);
        }
    }
}