﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenCVForUnity;

namespace recognTools
{

    public interface IRecieveRecognizedCode
    {
		void Recieve (string code);
    }
    public interface IRecieveRawImage
    {
        void Recieve (Mat sourceBgr);
    }
    public interface IRecieveFilteredImage
    {
        void Recieve (Mat sourceHsv);
    }
    public interface IRecieveFoundContours
    {
        void Recieve (List<MatOfPoint> contourList);
    }
    public interface IRecieveFailFilteredContours
    {
        void Recieve ();
    }
    public interface IRecieveCroptedImage
    {
        void Recieve (Mat sourceBgr);
    }

}
