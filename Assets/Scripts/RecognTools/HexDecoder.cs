﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenCVForUnity;
using UnityEngine;
using Quarcode.Core;
using System.IO;

namespace recognTools
{
    class HexDecoder
    {
        private static CPointsMatrix2 BitMatrix = null;
        /// <summary>
        /// Пытается найти валидируемый код на обрезанном по контору гекс кода изображении
        /// </summary>
        /// <param name="source">Cropted Hex Image</param>
        /// <returns> возвращает прочитанный валидированный код или null</returns>
        public static string TryDecode(Mat sourceBgr)
        {
            int threshold = 130;
            int r = 0, g = 0, b = 0;
            //размытие изображения для исключения шума
            //Imgproc.medianBlur(sourceBgr, sourceBgr, 5);

            CPointsMatrix matrix = new CPointsMatrix(sourceBgr.height());
            //Создаем матрицу точек
            BitMatrix = new CPointsMatrix2();
            //массив значений пикселей
            byte[] dst = new byte[sourceBgr.total() * sourceBgr.channels()];
            //заполняем массив значениями пикселей
            sourceBgr.get(0, 0, dst);
            //список значений бит, считанных из изображения
            List<bool> bitlist = new List<bool>();

            // K.O.S.T.J.L methods here


            // выраниваем гекс по центру
            Point[] data = Vector.ToSystemPointsF(BitMatrix.bitPoints.ToArray());

            double left = data.Select(x => x.x).Min();
            double top = data.Select(x => x.y).Min();

            double right = data.Select(x => x.x).Max();
            double bottom = data.Select(x => x.y).Max();

            // 66 и 76 - расстояния от верхних и боковых точек до границ. Взяты для картинки масштабом 
            double free_space_left = 76 / 680.0 * (right - left);
            double free_space_top = 66 / 680.0 * (bottom - top);
            // коэффициенты масштаба
            double k_x = sourceBgr.width() / (right - left + 2 * free_space_left);
            double k_y = sourceBgr.height() / (bottom - top + 2 * free_space_top);

            // масштабируем точки
            for (int i = 0; i < data.Length; i++)
            {
                data[i].x *= k_x;
                data[i].y *= k_y;
            }
            //обновлеяем левые и верхние значения
            left = data.Select(x => x.x).Min();
            top = data.Select(x => x.y).Min();

            // передвигаем начало координат в центр изображения
            Vector center = new Vector(left - free_space_left * k_x, top - free_space_top * k_y);
            for (int i = 0; i < data.Length; i++)
            {
                data[i].x -= center.x;
                data[i].y -= center.y;
            }
            // хз что это, видимо, не нужный элемент
            //Imgproc.circle(sourceBgr, new Point(0, 0), 5, new Scalar(200, 0, 0), 2);

            // end
            for (int i = 0; i < BitMatrix.bitPoints.Count; i++)
            {

                int ii = (int)data[i].y;
                int jj = (int)data[i].x;
                b = 0;
                g = 0;
                r = 0;

                // усредняем в квадрате 3х3
                for (int ishift = -0; ishift <= 0; ishift++)
                {
                    for (int jshift = -0; jshift <= 0; jshift++)
                    {

                        int where = ((ii + ishift) * sourceBgr.cols() + (jj + jshift)) * (int)sourceBgr.elemSize();

                        b += dst[where + 0];
                        g += dst[where + 1];
                        r += dst[where + 2];
                    }
                }
                // усредняем суммарные интенсивности
                b /= 1;
                g /= 1;
                r /= 1;
                // проверяем значения бита
                if (b > threshold && g > threshold && r > threshold)
                {
                    bitlist.Add(false);
                    //для отладки рисуется эллипс с белым и черным цветом для различным 
                    Imgproc.ellipse(sourceBgr, data[i], new Size(1, 1), 1.5f, 0f, 360f, new Scalar(0, 0, 0), 1);
                }
                else
                {
                    bitlist.Add(true);
                    Imgproc.ellipse(sourceBgr, data[i], new Size(1, 1), 1.5f, 0f, 360f, new Scalar(255, 255, 255), 1);
                }
                Imgproc.ellipse(sourceBgr, new Point(data[i].x, data[i].y), new Size(3, 3), 1.5f, 0f, 360f, new Scalar(0, 0, 255), 1);
                Imgproc.putText(sourceBgr, i.ToString(), new Point((int)data[i].x, (int)data[i].y), Core.FONT_HERSHEY_DUPLEX, 0.2f, new Scalar(0, 0, 0), 1);

            }
            //получаем полный символьный код из картинки
            string fullResult = CCoder.DeCode(bitlist);
            //часть, содержащая ID 
            string messageCandidate = fullResult.Substring(0, 16);
            //UnityEngine.Debug.Log("считан код: " + messageCandidate);
            //string md5Part = fullResult.Substring(16, 10);
            //страшный костыль. в силу кривой генерации на серваке сверяются только первые 9 символов
            //строка содержащая 10 первых символов md5 хэша от ID закодированных в картинке
            string md5Part = fullResult.Substring(16, 9);
            //вычисленный хэш
            string md5Calc = CCoder.GetMd5Sum(messageCandidate).Substring(1, 9);


            //UnityEngine.Debug.Log("ожидаемый хэш: " + md5Calc);
            //UnityEngine.Debug.Log("считанный хэш: " + md5Part);
            if (md5Calc.Equals(md5Part))
                UnityEngine.Debug.Log("ВЕРНЫЙ хэш:, код: " + messageCandidate);
            else
                UnityEngine.Debug.Log("НЕКОРРЕКТНЫЙ хэш, код: " + messageCandidate);
            if (md5Calc.Equals(md5Part))
                return messageCandidate;
            else
                return null;
        }

        public static Mat Filter(Mat sourceBgr)
        {
            // 1.Gauss 
            //source.SmoothGaussian(7);
            //source = source.SmoothMedian(9);
            // 2.Color filter
            byte[] data = new byte[sourceBgr.total() * sourceBgr.channels()];

            sourceBgr.get(0, 0, data);

            Mat TMP = new Mat(sourceBgr.height(), sourceBgr.width(), CvType.CV_8UC3);

            //source.Dispose();
            byte[] dst = new byte[TMP.total() * TMP.channels()];

            TMP.get(0, 0, dst);


            bool IsBlack;
            //const double blueProp = 1.8;
            //const double greeProp = 2.5;
            //const double redProp = 2.5;
            //const byte treshhold = 15;
            const int grayMax = 80; // превышение этого уровня по любому каналу означет отсутствие черного
            const byte zeroLevel = 100;//уровень выше которого цвет проверяется на отклонение от серого
            const byte colorDif = 70; // максимальное отклонение между цветами
            Mat filteredimagePre = new Mat(TMP.height(), TMP.width(), CvType.CV_8UC3);
            Mat filteredimageBgr = new Mat(TMP.height(), TMP.width(), CvType.CV_8UC3);
            Imgproc.cvtColor(sourceBgr, filteredimagePre, Imgproc.COLOR_BGR2GRAY);
            Imgproc.adaptiveThreshold(filteredimagePre, filteredimageBgr, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 11, 10);
            //Imgproc.dilate(filteredimageBgr, filteredimageBgr, new Mat(), new Point(), 3);

            #region old method
            //for (int i = TMP.rows() - 1; i >= 0; i--)
            //{
            //    for (int j = TMP.cols() - 1; j >= 0; j--)
            //    {
            //        IsBlack = true;
            //        // 0 - blue
            //        // 1 - green
            //        // 2 - red
            //        //new great, god like code
            //        //debug
            //        if (i == 847 && j == 1580)
            //        {
            //            //debug here
            //        }



            //        int where = (i * sourceBgr.cols() + j) * (int)sourceBgr.elemSize();


            //        byte b = data[where + 0];
            //        byte g = data[where + 1];
            //        byte r = data[where + 2];


            //        if (b > grayMax)
            //            IsBlack = false;
            //        if (g > grayMax)
            //            IsBlack = false;
            //        if (r > grayMax)
            //            IsBlack = false;
            //        if (b >= zeroLevel || g >= zeroLevel || r >= zeroLevel)
            //        {
            //            //too many blue
            //            //if (b > r + colorDif || b > g + colorDif)
            //            //    IsBlack = false;
            //            ////too many green
            //            //if (g > r + colorDif || g > b + colorDif)
            //            //    IsBlack = false;
            //            ////too many red
            //            //if (r > b + colorDif || r > g + colorDif)
            //            //    IsBlack = false;
            //        }

            //        if (IsBlack)
            //        {
            //            dst[where + 0] = 0;
            //            dst[where + 1] = 0;
            //            dst[where + 2] = 0;
            //        }
            //        else
            //        {
            //            dst[where + 0] = 255;
            //            dst[where + 1] = 255;
            //            dst[where + 2] = 255;
            //        }
            //    }
            //}
            #endregion
            // 3.To HSV
            //filteredimageBgr.put(0, 0, dst);

            //Imgproc.medianBlur(filteredimageBgr, filteredimageBgr, 5);
            Imgproc.cvtColor(filteredimageBgr, filteredimageBgr, Imgproc.COLOR_BGR2HSV_FULL);
            return filteredimageBgr;
        }

        public static List<MatOfPoint> FilterAllContours(List<MatOfPoint> source)
        {
            List<MatOfPoint> result = new List<MatOfPoint>();
            bool ready = false;
            List<MatOfPoint> contours = source;

            double[] Areas = new double[contours.Count];
            for (int i = 0; i < contours.Count; i++)
            {

                Areas[i] = Imgproc.contourArea(contours[i]);
            }

            for (int i = 0; i < contours.Count && !ready; i++)
            {

                MatOfPoint contour = contours[i];


                for (int j = 1; j < contours.Count && !ready; j++)
                {
                    if (Math.Abs(Areas[i] / Areas[j] - 0.33) < 0.15)
                    {
                        result.Add(contours[j]);
                        result.Add(contours[i]);
                        ready = !ready;
                    }
                }

            }
            return result;
        }

        public static List<MatOfPoint> FindAllContours(Mat sourceHsv)
        {
            Mat maskHsvBlack = new Mat();

            Scalar blueVal_min = new Scalar(0, 50, 125);
            Scalar blueVal_max = new Scalar(359.9, 255, 255);
            Scalar blackVal_min = new Scalar(0, 0, 100);
            Scalar blackVal_max = new Scalar(360, 255, 255);




            // borders
            //Core.inRange(sourceHsv, blackVal_min, blackVal_max, maskHsvBlack);

            Imgproc.erode(maskHsvBlack, maskHsvBlack, new Mat(), new Point(-1, -1), 3);

            Imgproc.dilate(maskHsvBlack, maskHsvBlack, new Mat(), new Point(-1, -1), 3);


            Core.bitwise_not(maskHsvBlack, maskHsvBlack);



            double cannyThreshold = 1.0;
            double cannyThresholdLinking = 500.0;


            Mat cannyBlack = new Mat();

            //Imgproc.Canny(maskHsvBlack, cannyBlack, cannyThreshold, cannyThresholdLinking);
            Imgproc.Canny(sourceHsv, cannyBlack, 50, 150);


            List<MatOfPoint> borders = new List<MatOfPoint>();//list of all borders


            int minimumArea = sourceHsv.height() * sourceHsv.width() / 80;


            List<MatOfPoint> contours = new List<MatOfPoint>();

            Imgproc.findContours(cannyBlack, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);


            foreach (MatOfPoint each in contours)
            {
                MatOfPoint contour = each;
                MatOfPoint2f approxContour = new MatOfPoint2f();
                Imgproc.approxPolyDP(new MatOfPoint2f(contour.toArray()), approxContour, Imgproc.arcLength(new MatOfPoint2f(contour.toArray()), true) * 0.05f, true);

                if (Imgproc.contourArea(contour, false) > minimumArea)
                {
                    borders.Add(contour);
                }

            }
            return borders;
        }

        public static Mat CropCodeFromImage(Mat sourceBgr, List<MatOfPoint> Contours)
        {
            Mat CroptedImage;
            int idMain = 0;
            int idBlue = 1;
            if (Imgproc.contourArea(Contours[0]) < Imgproc.contourArea(Contours[1]))
            {
                idMain = 1;
                idBlue = 0;
            }
            Point[] mainPoints = Contours[idMain].toArray();
            Point[] subPoints = Contours[idBlue].toArray();

            Mat RotatedImage = new Mat();

            int bottom = (int)mainPoints.Min(x => x.y);

            int left = (int)mainPoints.Min(x => x.x);

            int top = (int)mainPoints.Max(x => x.y);

            int right = (int)mainPoints.Max(x => x.x);

            OpenCVForUnity.Rect box = new OpenCVForUnity.Rect(left, bottom, right - left, top - bottom);
            CroptedImage = sourceBgr;
            // 1.Ищем угол поворота относительно вертикали для вертикального выравнивания контуров
            double Angle = 0;
            // 1.1.найдем две точки : самую левую основного контура и самую левую синего  контура
            Point mainLeft = (from p in mainPoints
                              orderby p.x
                              select p).FirstOrDefault();
            Point mainBottom = (from p in mainPoints
                                orderby p.y descending
                                select p).FirstOrDefault();
            Point subLeft = (from p in subPoints
                             orderby p.x
                             select p).FirstOrDefault();
            Point center = new Point(CroptedImage.width() / 2, CroptedImage.height() / 2);
            // 1.2 Ищем угол поворота, который поставит одну над второй
            if (mainLeft.x != subLeft.x)
            {
                double tg = -(mainLeft.y - subLeft.y) / (mainLeft.x - subLeft.x);

                Angle = Math.Atan(tg) + Math.PI / 2;
            }

            #region вертикальное выравнивание
            // случай перевернутого изображения
            mainLeft.x -= center.x;
            mainLeft.y -= center.y;
            // умножение на матрицу поворота
            Point old = new Point(mainLeft.x, mainLeft.y);
            //mainLeft.x = (int)(old.x * Math.Cos (Angle) - old.y * Math.Sin (Angle));
            //mainLeft.y = (int)(+old.x * Math.Sin (Angle) + old.y * Math.Cos (Angle));
            // обратный переход в координаты отрезанного изображения 
            mainLeft.x += center.x;
            mainLeft.y += center.y;
            // случай перевернутого изображения
            subLeft.x -= center.x;
            subLeft.y -= center.y;
            // умножение на матрицу поворота
            old = new Point(subLeft.x, subLeft.y);
            subLeft.x = (int)(old.x * Math.Cos(Angle) - old.y * Math.Sin(Angle));
            subLeft.y = (int)(+old.x * Math.Sin(Angle) + old.y * Math.Cos(Angle));
            // обратный переход в координаты отрезанного изображения 
            subLeft.x += center.x;
            subLeft.y += center.y;
            if (mainLeft.y > subLeft.y)
                Angle -= Math.PI;
            #endregion


            // 1.3 Поворачиваем основное изображение на этот угол
            /*DEBUG*/
            // Imgproc.rectangle (sourceBgr, new Point (box.x, box.y), new Point (box.x + box.width, box.y + box.height), new Scalar (0, 0, 255, 255), 3);


            Point centerCropImage = new Point(CroptedImage.cols() * 0.5f, CroptedImage.rows() * 0.5f);

            Mat affine_matrix = Imgproc.getRotationMatrix2D(centerCropImage, -(Angle * 180 / Math.PI), 1.0f);

            Imgproc.warpAffine(CroptedImage, RotatedImage, affine_matrix, CroptedImage.size(), Imgproc.INTER_CUBIC | Imgproc.WARP_FILL_OUTLIERS, 0, new Scalar(255, 255, 255));




            // 2 Поворачиваем контур на этот угол
            RotateContour(mainPoints, Angle, center,
              new Point(center.x + (RotatedImage.width() - CroptedImage.width()) / 2, center.y + (RotatedImage.height() - CroptedImage.height()) / 2));


            // 3.Вырезаем основной контур(повернутый) из обрезанного и повернутого изображения
            bottom = (int)mainPoints.Max(x => x.y);
            top = (int)mainPoints.Min(x => x.y);
            left = (int)mainPoints.Min(x => x.x);
            right = (int)mainPoints.Max(x => x.x);



            List<List<MatOfPoint>> contoursRoteated = new List<List<MatOfPoint>>();
            contoursRoteated.Add(new List<MatOfPoint>());
            contoursRoteated.Add(new List<MatOfPoint>());
            contoursRoteated[0].Add(new MatOfPoint(mainPoints));
            contoursRoteated[1].Add(new MatOfPoint(mainPoints));

            //Texture2D texture;
            //byte[] png;
            //string Path;

            //Imgproc.drawContours(RotatedImage, contoursRoteated[0], 0, new Scalar(0, 255, 255));

            //texture = new Texture2D(RotatedImage.cols(), RotatedImage.rows(), TextureFormat.RGBA32, false);
            //Utils.matToTexture2D(RotatedImage, texture);
            //png = texture.EncodeToPNG();
            //Path = Application.streamingAssetsPath + "/../../out/" + 0000 + "_rotated.png";
            //File.WriteAllBytes(@Path, png);

            // При некорректном контуре падает
            if (left < 0 || right < 0 || top > RotatedImage.height() || right > RotatedImage.width())
            {
                Imgproc.polylines(RotatedImage, contoursRoteated[0], true, new Scalar(0, 255, 255), 2);
                return RotatedImage;
            }
            else
            {
                return CropImage(RotatedImage, contoursRoteated);
            }
        }


        /// <summary>
        /// Альтернативная функция вырезания
        /// </summary>
        /// <param name="source"></param>
        /// <param name="contours"></param>
        /// <returns></returns>
        public static Mat CropImage(Mat source, List<List<MatOfPoint>> contours)
        {
            //additional reordering contours
            MatOfPoint code_contour;
            MatOfPoint blue_contour;

            if (contours.Count > 2)
            {
                code_contour = (contours[1])[0];
                blue_contour = (contours[2])[0];
            }
            else
            {
                code_contour = (contours[0])[0];
                blue_contour = (contours[1])[0];
            }

            MatOfPoint2f pointMat = new MatOfPoint2f();
            Imgproc.approxPolyDP(new MatOfPoint2f(code_contour.toArray()), pointMat, Imgproc.arcLength(new MatOfPoint2f(code_contour.toArray()), true) * 0.10f, true);
            code_contour = new MatOfPoint(pointMat.toArray());



            Imgproc.approxPolyDP(new MatOfPoint2f(blue_contour.toArray()), pointMat, Imgproc.arcLength(new MatOfPoint2f(blue_contour.toArray()), true) * 0.10f, true);
            blue_contour = new MatOfPoint(pointMat.toArray());




            Moments moments = Imgproc.moments(code_contour);
            Point Code_gravityCenter = new Point((int)(moments.m10 / moments.m00), (int)(moments.m01 / moments.m00));
            moments = Imgproc.moments(blue_contour);
            Point Blue_gravityCenter = new Point((int)(moments.m10 / moments.m00), (int)(moments.m01 / moments.m00));

            //corners
            Point[] corners_mainBox = code_contour.toArray();

            //sort corners
            List<Point> left = new List<Point>();
            List<Point> right = new List<Point>();
            left.AddRange(corners_mainBox.OrderBy(x => x.x).Take(2));
            right.AddRange(corners_mainBox.OrderBy(x => -x.x).Take(2));
            //for (int i = 0; i < corners_mainBox.Length; i++)
            //{
            //    if (corners_mainBox[i].x < Code_gravityCenter.x)
            //        left.Add(corners_mainBox[i]);
            //    else
            //        right.Add(corners_mainBox[i]);
            //}

            //matrix with starting Points
            Point[] corners = new Point[4];

            // top-left top-right bottom-right bottom-left
            corners[0] = left[0].y < left[1].y ? left[0] : left[1]; //top-left
            corners[1] = right[0].y < right[1].y ? right[0] : right[1]; //top-right
            corners[2] = right[0].y > right[1].y ? right[0] : right[1]; //bottom-right
            corners[3] = left[0].y > left[1].y ? left[0] : left[1]; //bottom-left


            OpenCVForUnity.Rect box = Imgproc.boundingRect(new MatOfPoint(code_contour));
            Point[] targetPF = {
                new Point (0, 0),
                new Point (box.width, 0),
                new Point (box.width, box.height),
                new Point (0, box.height)
            };
            //matrix with destination Points
            //rotate crop
            Mat newcroppimg = source.submat(box);

            foreach (Point p in corners)
            {
                p.x -= box.x;
                p.y -= box.y;
            }

            Mat src_mat = Converters.vector_Point2f_to_Mat(corners.ToList());


            Mat dst_mat = Converters.vector_Point2f_to_Mat(targetPF.ToList());

            Mat TransformMat = Imgproc.getPerspectiveTransform(src_mat, dst_mat);

            Size ROI = Imgproc.boundingRect(code_contour).size();


            Imgproc.warpPerspective(newcroppimg, newcroppimg, TransformMat, ROI);


            return newcroppimg;
        }

        private static void RotateContour(Point[] src, double Angle, Point firstC, Point secondC)
        {
            for (int i = 0; i < src.Length; i++)
            {
                // переход в систему координат центра 
                src[i].x -= firstC.x;
                src[i].y -= firstC.y;
                // умножение на матрицу поворота
                Point old = new Point(src[i].x, src[i].y);
                src[i].x = (int)(old.x * Math.Cos(Angle) - old.y * Math.Sin(Angle));
                src[i].y = (int)(+old.x * Math.Sin(Angle) + old.y * Math.Cos(Angle));
                // обратный переход в координаты нового центра 
                src[i].x += secondC.x;
                src[i].y += secondC.y;

            }
        }
    }
}