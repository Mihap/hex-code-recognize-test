﻿using UnityEngine;
using System.Collections.Generic;
using System;
using OpenCVForUnity;
using recognTools;
using Quarcode.Core;

public class HexcodeScanner : MonoBehaviour, IRecieveCroptedImage, IRecieveFilteredImage, IRecieveFoundContours, IRecieveRawImage, IRecieveRecognizedCode
{

  public Texture2D imgTexture;
  private Mat _rawImage;


  // Use this for initialization
  void Start()
  {
    _rawImage = new Mat(imgTexture.height, imgTexture.width, CvType.CV_8UC3);
    Utils.texture2DToMat(imgTexture, _rawImage);

    ImageParser.AddDevDataReciever(this);
    ImageParser.RecieveImage(_rawImage);
  }

  void IRecieveCroptedImage.Recieve(Mat source)
  {
    Debug.Log("cropted succesful");
    OutputImage(source);
  }

  void IRecieveFilteredImage.Recieve(Mat source)
  {
  }

  void IRecieveFoundContours.Recieve(List<MatOfPoint> contourList)
  {
    Imgproc.drawContours(_rawImage, contourList, -1, new Scalar(0, 255, 0), 3);
    OutputImage(_rawImage);

  }

  void IRecieveRawImage.Recieve(Mat source)
  {
  }

  void IRecieveRecognizedCode.Recieve(string code)
  {
    string result = string.Format("Code: {0}", code);
    Debug.Log(result);
  }


  void OutputImage(Mat mat)
  {
    Texture2D texture = new Texture2D(mat.cols(), mat.rows(), TextureFormat.RGBA32, false);
    Utils.matToTexture2D(mat, texture);
    gameObject.GetComponent<Renderer>().material.mainTexture = texture;
  }
}
